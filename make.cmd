SET gcc64_basedir=d:\etc\CodeBlocks\MinGW\bin
SET gcc32_basedir=d:\etc\MinGW32\bin

%gcc64_basedir%\mingw32-make.exe clean GCC_DIR=%gcc64_basedir% TARGET=x64 release
%gcc32_basedir%\mingw32-make.exe clean GCC_DIR=%gcc32_basedir% TARGET=x32 release
