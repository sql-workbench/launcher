/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2022, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
#include <string>
#include <string.h>
#include <time.h>
#include <windows.h>
#include <stdio.h>
#include "Log.h"
#include "utils.h"

static std::string logFileName;
static bool writeLogfile;

void Log::setWriteLogFile(bool flag)
{
  writeLogfile = flag;
}

void Log::setLogfileName(std::string fname)
{
  logFileName = fname;
}

void Log::logMsg(std::string msg)
{
  if (!writeLogfile) return;
  Log::writeMsg(msg);
}

void Log::writeMsg(std::string msg)
{
  char nowStr[strlen("yyyy-mm-dd hh:mm:ss") + 1];
  struct tm *timeinfo;
  time_t rawtime;
  time (&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(nowStr, sizeof(nowStr),"%Y-%m-%d %H:%M:%S", timeinfo);

  FILE *file = fopen(logFileName.c_str(), "a");
  if (file)
  {
    fprintf(file, "%s - %s\n", nowStr, msg.c_str());
    fclose(file);
  }
}

void Log::logError(std::string errorMsg)
{
  Log::logMsg("ERROR: " + errorMsg);
}

void Log::logJavaError(std::string msg, JNIEnv *env)
{
  if (!writeLogfile) return;

  jthrowable javaError = env->ExceptionOccurred();
  if (javaError)
  {
    jclass throwableClass = env->FindClass ("java/lang/Throwable");
    jmethodID getMsgMethod = env->GetMethodID(throwableClass, "getMessage", "()Ljava/lang/String;");
    jstring message(static_cast<jstring>(env->CallObjectMethod(javaError, getMsgMethod)));
    char const* utfMessage(env->GetStringUTFChars(message, 0));
    Log::logMsg(msg + ": " + std::string(utfMessage));
    env->ReleaseStringUTFChars(message, utfMessage);
    env->ExceptionDescribe();
  }
  else
  {
    Log::logError(msg);
  }
}

bool Log::canWrite(std::string fname)
{
  HANDLE hndl = CreateFile(fname.c_str(), GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, NULL);
  bool removeFile = false;

  if (GetLastError() == ERROR_FILE_NOT_FOUND)
  {
    hndl = CreateFile(fname.c_str(), GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_FLAG_BACKUP_SEMANTICS, NULL);
    removeFile = true;
  }

  bool result = INVALID_HANDLE_VALUE != hndl;

  if (result)
  {
    CloseHandle(hndl);
  }

  if (removeFile)
  {
    remove(fname.c_str());
  }
  return result;
}

void Log::deleteLogfile()
{
  if (!writeLogfile) return;
  remove(logFileName.c_str());
}
