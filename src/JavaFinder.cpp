/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2022, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
#include <string>
#include <stdexcept>

#include <windows.h>
#include <winreg.h>
#include <winver.h>

#include "utils.h"
#include "JavaFinder.h"
#include "Log.h"

#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383

using namespace std;

const int MIN_VERSION = VERSION(21,0);

JavaFinder::JavaFinder()
{
}

JavaFinder::JavaFinder(string basedir, string userSuppliedHome)
{
  exeDir = basedir;
  is64BitExe = (INTPTR_MAX == INT64_MAX);
  boolean valid = false;
  if (containsJava(userSuppliedHome))
  {
    int version = getJavaVersion(userSuppliedHome);
    if (version >= MIN_VERSION)
    {
      javaHome = userSuppliedHome;
    }
    else
    {
      Log::logError("Provided Java home: " + userSuppliedHome + " does not contain a Java 11 installation");
    }
  }
  if (javaHome.empty())
  {
    findJava();
  }
  buildBinaryPath();
}

Location JavaFinder::getFoundLocation()
{
  return foundLocation;
}

string JavaFinder::getBinPath()
{
  return binPath;
}

string JavaFinder::getJvmDLL()
{
  return jvmDLL;
}

string JavaFinder::getJavaExe()
{
  return javaExe;
}

string JavaFinder::getJavaHome()
{
  return javaHome;
}

bool JavaFinder::is64BitJava()
{
  if (!jvmDLL.empty())
  {
    return is64BitFile(jvmDLL);
  }
  return is64BitFile(javaExe);
}

bool JavaFinder::canStartInProcJVM()
{
  return (!jvmDLL.empty());
}

bool JavaFinder::setJavaHome(string newHome)
{
  if (containsJava(newHome))
  {
    javaHome = newHome;
    buildBinaryPath();
    return true;
  }
  return false;
}


// private methods

void JavaFinder::buildBinaryPath()
{
  if (javaHome.empty()) return;

  string dll;
  if (FileExists(javaHome + "\\bin\\server\\jvm.dll"))
  {
    dll = javaHome + "\\bin\\server\\jvm.dll";
    binPath = javaHome + "\\bin";
  }
  else if (FileExists(javaHome + "\\bin\\client\\jvm.dll") )
  {
    dll = javaHome + "\\bin\\client\\jvm.dll";
    binPath = javaHome + "\\bin";
  }
  else if (FileExists(javaHome + "\\jre\\bin\\server\\jvm.dll"))
  {
    dll = javaHome + "\\jre\\bin\\server\\jvm.dll";
    binPath = javaHome + "\\jre\\bin";
  }
  else if (FileExists(javaHome + "\\jre\\bin\\client\\jvm.dll"))
  {
    dll = javaHome + "\\jre\\bin\\client\\jvm.dll";
    binPath = javaHome + "\\jre\\bin";
  }

  if (canUseInProcJVM(dll))
  {
    jvmDLL = dll;
  }
  else
  {
    jvmDLL.clear();
    binPath.clear();
  }

  if (FileExists(javaHome + "\\bin\\javaw.exe"))
  {
    javaExe = javaHome + "\\bin\\javaw.exe";
  }
  else if (FileExists(javaHome + "\\jre\\bin\\javaw.exe"))
  {
    javaExe = javaHome + "\\jre\\bin\\javaw.exe";
  }
}

bool JavaFinder::findJava()
{
  if (containsJava(exeDir + "\\jre"))
  {
    if (getJavaVersion(exeDir + "\\jre") >= MIN_VERSION)
    {
      javaHome = exeDir + "\\jre";
      foundLocation = localJRE;
      return true;
    }
    Log::logError("Local Java runtime at \"" + exeDir + "\\jre\" is not a Java 11 runtime");
  }

  char const* wbjdk = getenv("WORKBENCH_JDK");
  if (wbjdk != NULL)
  {
    string wbhome = string(wbjdk);
    if (containsJava(wbhome))
    {
      if (getJavaVersion(wbhome) >= MIN_VERSION)
      {
        javaHome = wbhome;
        foundLocation = wbEnv;
        return true;
      }
      Log::logError("Java runtime specified through WORKBENCH_JDK at \"" + wbhome + "\" is not a Java 21 runtime");
    }
  }

  char const* jhome = getenv("JAVA_HOME");
  if (jhome != NULL)
  {
    string home = string(jhome);
    if (containsJava(home))
    {
      if (getJavaVersion(home) >= MIN_VERSION)
      {
        javaHome = home;
        foundLocation = javaHomeEnv;
        return true;
      }
      Log::logError("Java runtime specified through JAVA_HOME at \"" + home + "\" is not a Java 11 runtime");      
    }
  }

  string regHome = getJavaHomeFromRegistry();
  if (!regHome.empty())
  {
    javaHome = regHome;
    foundLocation = Location::registry;
    return true;
  }

  string pathHome = searchPath();
  if (!pathHome.empty())
  {
    foundLocation = Location::systemPath;
    javaHome = pathHome;
  }
  return false;
}

string JavaFinder::getJavaHomeFromRegistry()
{
  string result;
  HKEY hTestKey;

  LPCSTR baseKey = "SOFTWARE\\JavaSoft\\Java Runtime Environment";
  if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, baseKey, 0, KEY_READ, &hTestKey) == ERROR_SUCCESS)
  {
    result = readRegistry(hTestKey);
  }
  RegCloseKey(hTestKey);

  // If this is the 64bit launcher, also test for the 32bit registry
  if (result.empty() && is64BitExe)
  {
    baseKey = "SOFTWARE\\Wow6432Node\\JavaSoft\\Java Runtime Environment";
    if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, baseKey, 0, KEY_READ, &hTestKey) == ERROR_SUCCESS)
    {
      result = readRegistry(hTestKey);
    }
    RegCloseKey(hTestKey);
  }
  return result;
}

string JavaFinder::readRegistry(HKEY hKey)
{
  // Taken from https://docs.microsoft.com/en-us/windows/win32/sysinfo/enumerating-registry-subkeys
  TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
  DWORD    cbName;                   // size of name string
  TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name
  DWORD    cchClassName = MAX_PATH;  // size of class string
  DWORD    cSubKeys=0;               // number of subkeys
  DWORD    cbMaxSubKey;              // longest subkey size
  DWORD    cchMaxClass;              // longest class string
  DWORD    cValues;              // number of values for key
  DWORD    cchMaxValue;          // longest value name
  DWORD    cbMaxValueData;       // longest value data
  DWORD    cbSecurityDescriptor; // size of security descriptor
  FILETIME ftLastWriteTime;      // last write time

  DWORD i, retCode;

  TCHAR achValue[MAX_VALUE_NAME];
  DWORD cchValue = MAX_VALUE_NAME;

  int maxVersion = 0;
  string maxVersionKey;
  string home;

  // Get the class name and the value count.
  retCode = RegQueryInfoKey(
      hKey,                    // key handle
      achClass,                // buffer for class name
      &cchClassName,           // size of class string
      NULL,                    // reserved
      &cSubKeys,               // number of subkeys
      &cbMaxSubKey,            // longest subkey size
      &cchMaxClass,            // longest class string
      &cValues,                // number of values for this key
      &cchMaxValue,            // longest value name
      &cbMaxValueData,         // longest value data
      &cbSecurityDescriptor,   // security descriptor
      &ftLastWriteTime);       // last write time

  if (cSubKeys)
  {
    for (i=0; i<cSubKeys; i++)
    {
      cbName = MAX_KEY_LENGTH;
      retCode = RegEnumKeyEx(hKey, i, achKey, &cbName, NULL, NULL, NULL, &ftLastWriteTime);
      if (retCode == ERROR_SUCCESS)
      {
        int version = parseVersionString(achKey);
        if (version < MIN_VERSION) continue;

        if (version > maxVersion)
        {
          maxVersion = version;
          maxVersionKey = achKey;
        }
      }
    }
  }

  if (!maxVersionKey.empty())
  {
    retCode = RegGetValue(hKey, maxVersionKey.c_str(), TEXT("JavaHome"), RRF_RT_REG_SZ, NULL, &achValue, &cchValue);
    if (retCode == ERROR_SUCCESS)
    {
      home = achValue;
    }
  }
  return home;
}

int JavaFinder::getJavaVersion(string home)
{
  if (FileExists( (home + "\\bin\\java.exe").c_str() ))
  {
    return getFileVersion(home + "\\bin\\java.exe");
  }

  if (FileExists( (home + "\\jre\\bin\\java.exe").c_str() ))
  {
    return getFileVersion(home + "\\jre\\bin\\java.exe");
  }
  return 0;
}

string JavaFinder::searchPath()
{
  list<string> path = split(getenv("PATH"), ';');
  for (string dir : path)
  {
    string exe = dir + "\\java.exe";
    if (FileExists(exe))
    {
      int version = getFileVersion(exe);
      if (version >= MIN_VERSION)
      {
        // strip the bin dir from the path
        int pos = dir.find("\\bin");
        if (pos > 0)
        {
          dir = dir.substr(0,pos);
          // Deal with the JRE sub-directory of a  JDK
          pos = dir.find("\\jre");
          if (pos > 0)
          {
            dir = dir.substr(0,pos);
          }
        }
        return dir;
      }
    }
  }
  return "";
}

list<string> JavaFinder::split(string input, char delimiter)
{
  char *token;
  list<string> tokens;

  token = strtok(strdup(input.c_str()), ";");
  while (token != NULL)
  {
    tokens.push_back(string(token));
    token = strtok(NULL, ";");
  }
  return tokens;
}

int JavaFinder::getFileVersion(string fname)
{
  DWORD handle = 0;
  size_t infoSize = GetFileVersionInfoSize(fname.c_str(), &handle);
  BYTE* versionInfo = new BYTE[infoSize];

  if (!GetFileVersionInfo(fname.c_str(), handle, infoSize, versionInfo))
  {
    // No version information in the file
    delete[] versionInfo;
    return 0;
  }

  UINT len = 0;
  VS_FIXEDFILEINFO*   vsfi = NULL;
  VerQueryValue(versionInfo, TEXT("\\"), (void**)&vsfi, &len);
  WORD major = HIWORD(vsfi->dwFileVersionMS);
  WORD minor = LOWORD(vsfi->dwFileVersionMS);

  // don't know if this can happen
  if (major == 1 && minor >= 8)
  {
    major = 8;
    minor = 0;
  }
  delete[] versionInfo;

  return VERSION(major, minor);
}

bool JavaFinder::containsJava(string homeDir)
{
  if (FileExists(homeDir + "\\bin\\java.exe")) return true;
  if (FileExists(homeDir + "\\jre\\bin\\java.exe")) return true;

  return false;
}

bool JavaFinder::canUseInProcJVM(string dll)
{
  if (dll.empty()) return false;
  int bits = getDllType(dll.c_str());
  Log::logMsg(dll + " is a " + to_string(bits) + "bit dll");
  if (is64BitExe) return bits == 64;
  return bits == 32;
}

bool JavaFinder::is64BitFile(string fileName)
{
  int bits = getDllType(fileName.c_str());
  return bits == 64;
}

int JavaFinder::parseVersionString(string version)
{
  if (version.empty()) return 0;

  try
  {
    int pos = version.find(".");
    string major;
    string minor;
    if (pos > -1)
    {
      major = version.substr(0, pos);
      if (major.compare("1") == 0)
      {
        major = version.substr(pos + 1);
        minor = "0";
      }
      else
      {
        minor = version.substr(pos + 1);
      }
    }
    else
    {
      major = version;
      minor = "0";
    }

    pos = minor.find(".");
    if (pos > -1)
    {
      minor = minor.substr(0, pos);
    }
    return VERSION(stoi(major), stoi(minor));
  }
  catch (...)
  {
    return 0;
  }
}
