/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2022, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
// gcc with a 32bit target does not support std::thread
#ifdef WIN32_TARGET
#include "mingw.thread.h"
#else
#include <thread>
#endif // WIN32_TARGET

#include <string>
#include <future>

#include <windows.h>
#include <winuser.h>
#include <commctrl.h>
#include "resource.h"
#include "DialogChoice.h"
#include "Downloader.h"
#include "Unzipper.h"

static SelectionDialog * dialog = 0;

void doDownload(HWND hwnd, std::string dir, bool useWbDownload)
{
  string filename = dir + "\\jre.zip";
  Downloader downloader(useWbDownload);
  bool success = downloader.downloadJRE(filename);
  if (success)
  {
    string msg = "Unpacking archive to " + dir;
    SetWindowText(GetDlgItem(hwnd, TXTID_MESSAGE), msg.c_str());
    Yield();
    Unzipper unzip(filename);
    if (useWbDownload)
    {
      unzip.unzipTo(dir + "\\jre");
    }
    else if (unzip.singleSubDir())
    {
      string extractedDir = unzip.unzipTo(dir);
      string fullDir = dir + "\\" + extractedDir;
      MoveFile(fullDir.c_str(), (dir + "\\jre").c_str());
    }
    else
    {
      string msg = "Could not unpack the downloaded archive " + filename;
      MessageBox(NULL, msg.c_str(), "SQL Workbench/J", MB_OK | MB_ICONERROR);
      dialog -> selectedAction = ACTION_CANCEL;
    }
  }
  else
  {
    MessageBox(NULL, "Could not download Java runtime", "SQL Workbench/J", MB_OK | MB_ICONERROR);
    dialog -> selectedAction = ACTION_CANCEL;
  }
  PostMessage(hwnd, WM_CLOSE, 0, 0);
}

void downloadAndUnzip(HWND hwnd, std::string dir, bool useWbDownload)
{
  std::string message;
  if (useWbDownload)
  {
    message = "Downloading JRE from www.sql-workbench.eu to: " + dir;
  }
  else
  {
    message = "Downloading JRE from AdoptOpenJDK to: " + dir;
  }
  SetWindowText(GetDlgItem(hwnd, TXTID_MESSAGE), message.c_str());
  EnableWindow(GetDlgItem(hwnd, IDDOWNLOAD), false);
  EnableWindow(GetDlgItem(hwnd, IDSELECT), false);
  std::thread worker(doDownload, hwnd, dir, useWbDownload);
  worker.detach();
}

BOOL CALLBACK SelectionDialogCallback(HWND hwnd, UINT message, WPARAM wp, LPARAM lp)
{
  static HCURSOR waitCursor = NULL;
  static HCURSOR originalCursor = NULL;

  switch (message)
  {
    case WM_INITDIALOG:
      HANDLE hIcon;
      dialog = (SelectionDialog *) lp;
      waitCursor = LoadCursor(NULL, IDC_WAIT);
      hIcon = LoadImage(GetModuleHandle(0), MAKEINTRESOURCE(MAINICON), IMAGE_ICON, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), 0);
      if (hIcon)
      {
        SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);
      }
      return TRUE;

    case WM_SETCURSOR:
      if (LOWORD(lp) == HTCLIENT)
      {
        if (dialog && (dialog -> isDownloading) == 1)
        {
          HCURSOR old = SetCursor(waitCursor);
          if (originalCursor == 0)
          {
            originalCursor = old;
          }
        }
        else if (originalCursor != 0)
        {
           SetCursor(originalCursor);
        }
        return TRUE;
      }
      break;

    case WM_DESTROY:
      PostQuitMessage(0);
      return TRUE;

    case WM_CLOSE:
      DestroyWindow(hwnd);
      return TRUE;

    case WM_COMMAND:
      int event = HIWORD(wp);
      if (event == BN_CLICKED)
      {
        int ctrl = LOWORD(wp);
        if (ctrl == IDSELECT)
        {
          dialog -> selectedAction = ACTION_SELECT_HOME;
          DestroyWindow (hwnd);
          return TRUE;
        }
        else if (ctrl == IDDOWNLOAD)
        {
          dialog -> isDownloading = 1;
          dialog -> selectedAction = ACTION_DOWNLOAD;
          downloadAndUnzip(hwnd, dialog -> baseDir, dialog -> useWbDownload);
          return TRUE;
        }
        else // Cancel
        {
          dialog -> selectedAction = ACTION_CANCEL;
          PostQuitMessage(0);
          return TRUE;
        }
      }
  }
  return FALSE;
}

int SelectionDialog::selectAction()
{
  selectedAction = ACTION_CANCEL;

  HWND dlg = CreateDialogParam(GetModuleHandle(0), MAKEINTRESOURCE(ID_ACTION_DIALOG), 0, SelectionDialogCallback, (LPARAM) this);
  MSG msg;

  while (GetMessage( &msg, 0, 0, 0 ) )
  {
    if ( !IsDialogMessage( dlg, &msg ) )
    {
      TranslateMessage( &msg );
      DispatchMessage( &msg );
    }
  }
  return selectedAction;
}


