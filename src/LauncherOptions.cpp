/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2022, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
#include <string>
#include <list>
#include <algorithm>

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <Processenv.h>

#include "LauncherOptions.h"
#include "JavaFinder.h"
#include "Log.h"
#include "utils.h"

using namespace std;

const string ARG_NO_MEM = "nomemory";
const string ARG_SINGLE_INSTANCE = "singleinstance";
const string ARG_JAVA_HOME = "javahome";
const string ARG_WRITE_LOG = "logstartup";
const string ARG_JAR_DIR = "jardir";
const string ARG_USE_WB_JRE = "wbjre";

const string ARG_IN_PROCESS = "inProcessVM";
const string ARG_WB_CONFIG_DIR = "configdir";

const string CFG_FILE_NAME = "SQLWorkbench.cfg";
const string JAR_FILE_NAME = "sqlworkbench.jar";
const string DEFAULT_LOG_FILE_NAME = "wblauncher.log";

LauncherOptions::LauncherOptions()
{
}

LauncherOptions::LauncherOptions(int argc, char* argv[])
{
  char filename[MAX_PATH];
  GetModuleFileName(NULL, filename, MAX_PATH);
  string exename = string(filename);
  int pos = exename.find_last_of("\\");
  baseDir = exename.substr(0,pos);
  baseName = getBaseName(exename);
  bool defaultName = true;
  string logBaseName;

  wbConfigDir = string(getenv("USERPROFILE"));
  string wbSettings = baseDir + "\\workbench.settings";
  if (FileExists(wbSettings))
  {
    wbConfigDir = baseDir;
  }

  if (startsWithIgnoreCase(baseName, "sqlworkbench"))
  {
    logBaseName = DEFAULT_LOG_FILE_NAME;
  }
  else
  {
    defaultName = false;
    logBaseName = baseName + "_launcher.log";
  }

  bool deleteLogFile = true;
  bool writeLogFile = true;
  bool automaticMemory = true;

  for (int i=1; i < argc; i++)
  {
    string arg = string(argv[i]);
    pos = arg.find("=");
    string argName = arg;
    string orgName = arg;
    string argValue = arg;

    if (pos > -1)
    {
      argName = arg.substr(0, pos);
      orgName = argName;
      std::transform(argName.begin(), argName.end(), argName.begin(), ::tolower);
      argValue = expandEnv(arg.substr(pos + 1));
    }

    if (argName.at(0) == '-')
    {
      argName = argName.substr(1); // remove the leading dash
    }

    if (argName.compare(ARG_JAVA_HOME) == 0)
    {
      javaHome = argValue;
    }
    else if (argName.compare(ARG_NO_MEM) == 0)
    {
      automaticMemory = false;
    }
    else if (argName.compare(ARG_SINGLE_INSTANCE) == 0)
    {
      singleInstance = argValue.compare("true") == 0;
    }
    else if (argName.compare(ARG_WRITE_LOG) == 0)
    {
      writeLogFile = argValue.compare("false") != 0;
      if (argValue.compare("append") == 0)
      {
        deleteLogFile = false;
      }
    }
    else if (startsWithIgnoreCase(argName, ARG_USE_WB_JRE))
    {
      workbenchJreDownload = argValue.compare("true") == 0;
    }
    else if (startsWithIgnoreCase(argName, ARG_IN_PROCESS))
    {
      startInProcessJVM = argValue.compare("true") == 0;
    }
    else if (startsWithIgnoreCase(argName, ARG_JAR_DIR))
    {
      baseDir = argValue;
    }
    else if (orgName.substr(0,2).compare("-X") == 0 || orgName.substr(0,2).compare("-D") == 0)
    {
      vmOptions.push_back(arg);
      if (orgName.substr(0,4).compare("-Xmx") == 0)
      {
        memorySpecified = true;
      }
    }
    else
    {
      if (startsWithIgnoreCase(argName, ARG_WB_CONFIG_DIR))
      {
        wbConfigDir = argValue;
      }
      if (arg.find(" ") != string::npos)
      {
        arg = orgName + "=\"" + argValue + "\"";
      }
      wbOptions.push_back(arg);
    }
  }

  configFileName = baseDir + "\\" + baseName + ".cfg";
  if (!defaultName && !FileExists(configFileName.c_str()))
  {
    // no config file with the name of the customized exe.
    // fall back to the default config file
    configFileName = baseDir + "\\" + CFG_FILE_NAME;
  }

  mainJarFile =  baseDir + "\\" + JAR_FILE_NAME;

  std::string fname = baseDir + "\\" + logBaseName;
  bool writeable = Log::canWrite(fname);
  if (writeable == false)
  {
    // if the EXE directory isn't writeable, create the startup log in the config directory
    fname = wbConfigDir + "\\"  + logBaseName;
    writeable = Log::canWrite(fname);
    if (writeable == false)
    {
      // if the config directory isn't writeable either, create the startup log in the
      // user's home directory
      char *profile = getenv("USERPROFILE");
      fname = std::string(profile) + "\\" + logBaseName;
    }
  }

  Log::setLogfileName(fname);
  Log::setWriteLogFile(writeLogFile);

  if (deleteLogFile)
  {
    Log::deleteLogfile();
  }

  vmOptions.push_back("-Dvisualvm.display.name=SQLWorkbench/J");
  vmOptions.push_back("--add-opens=java.desktop/com.sun.java.swing.plaf.windows=ALL-UNNAMED");
  vmOptions.push_back("-Xverify:none");

  string bits (INTPTR_MAX == INT64_MAX ? "64" : "32");
  Log::logMsg("=== Starting " + bits + "bit SQL Workbench/J launcher ===");
  Log::logMsg("Base directory: " + baseDir);
  Log::logMsg("Launcher config file: " + configFileName);

  convertOldINI();
  readConfigFile();
  javaFinder = JavaFinder(baseDir, javaHome);

  if (javaFinder.getFoundLocation() != Location::unknown)
  {
    switch (javaFinder.getFoundLocation())
    {
      case localJRE:
        Log::logMsg("Using local JRE: " + javaFinder.getJavaHome());
        break;
      case wbEnv:
        Log::logMsg("Using Java specified by WORKENCH_JDK: " + javaFinder.getJavaHome());
        break;
      case javaHomeEnv:
        Log::logMsg("Using Java specified by JAVA_HOME: " + javaFinder.getJavaHome());
        break;
      case registry:
        Log::logMsg("Using Java from registry: " + javaFinder.getJavaHome());
        break;
      case systemPath:
        Log::logMsg("Using Java from PATH:" + javaFinder.getJavaHome());
        break;
      default:
        break;
    }
  }

  if (automaticMemory)
  {
    calculateMemory();
  }
}

string LauncherOptions::getMutexName()
{
  string clean = wbConfigDir;
  std::replace(clean.begin(), clean.end(), '\\', '/');
  std::replace(clean.begin(), clean.end(), ':', '/');
  return "sqlwb$" + clean + "/Mutex";
}

string LauncherOptions::getBaseName(string exename)
{
  string fname = exename;
  size_t pos = fname.find_last_of("\\");

  if (std::string::npos != pos)
  {
    fname.erase(0, pos + 1);
  }

  // Remove extension if present.
  pos = fname.rfind('.');
  if (std::string::npos != pos)
  {
    fname.erase(pos);
  }

  // Remove trailing 64/32 bit indicator
  pos = fname.rfind("64");
  if (std::string::npos != pos)
  {
    fname.erase(pos);
  }

  pos = fname.rfind("32");
  if (std::string::npos != pos)
  {
    fname.erase(pos);
  }

  return fname;
}

bool LauncherOptions::useWorkbenchJreDownload()
{
  return workbenchJreDownload;
}

bool LauncherOptions::useSingleInstance()
{
  return singleInstance;
}

bool LauncherOptions::shouldStartInProcessVM()
{
  return startInProcessJVM;
}

string  LauncherOptions::expandEnv(string text)
{
  char result[MAX_PATH + 1];
  ExpandEnvironmentStrings(text.c_str(), result, MAX_PATH);
  return string(result);
}

string LauncherOptions::getValue(string input)
{
  int pos = input.find("=");
  if (pos < 0) return input;
  return expandEnv(input.substr(pos + 1));
}

bool LauncherOptions::startsWith(string input, string toCheck)
{
  if (input.empty()) return false;
  if (toCheck.empty()) return false;
  return input.substr(0, toCheck.size()).compare(toCheck) == 0;
}

bool LauncherOptions::startsWithIgnoreCase(string input, string toCheck)
{
  if (input.empty()) return false;
  if (toCheck.empty()) return false;

  string toCheckLower = toCheck;
  std::transform(toCheckLower.begin(), toCheckLower.end(), toCheckLower.begin(), ::tolower);

  string start = input.substr(0, toCheck.size());
  std::transform(start.begin(), start.end(), start.begin(), ::tolower);

  return start.compare(toCheckLower) == 0;
}

list<string> LauncherOptions::getVmOptions()
{
  return vmOptions;
}

list<string> LauncherOptions::getWbOptions()
{
  return wbOptions;
}

string LauncherOptions::getBaseDir()
{
  return baseDir;
}

void LauncherOptions::readConfigFile()
{
  FILE *handle = fopen(configFileName.c_str(), "r");

  if (handle != NULL)
  {
    char lineBuffer[1024];

    while( fgets(lineBuffer, 1024, handle) != NULL)
    {
      // remove newlines at end of buffer
      char* ptr = strchr(lineBuffer, '\r');
      if (ptr) *ptr = '\0';
      ptr = strchr(lineBuffer, '\n');
      if (ptr) *ptr = '\0';

      string line = string(lineBuffer);
      if (startsWithIgnoreCase(line, ARG_JAVA_HOME) && javaHome.empty())
      {
        javaHome = getValue(line);
        Log::logMsg("Using Java home from config file: " + javaHome);
      }
      else if (startsWithIgnoreCase(line, ARG_SINGLE_INSTANCE))
      {
        singleInstance = getValue(line).compare("true") == 0;
      }

      if (startsWithIgnoreCase(line, ARG_IN_PROCESS))
      {
        startInProcessJVM = getValue(line).compare("true") == 0;
        Log::logMsg("Disabling in-process JVM because of config file entry: " + line);
      }

      if (startsWithIgnoreCase(line, "vmarg."))
      {
        string value = getValue(line);
        if (startsWith(value, "-Xmx"))
        {
          // Don't use the value from the ini file if the memory was specified on the command line
          if (!memorySpecified)
          {
            Log::logMsg("Using memory setting from config file");
            memorySpecified = true;
            vmOptions.push_back(value);
          }
        }
        else
        {
          vmOptions.push_back(value);
        }
      }

      if (startsWithIgnoreCase(line, "arg."))
      {
        string wbArg = getValue(line);
        if (startsWithIgnoreCase(wbArg, ARG_WB_CONFIG_DIR))
        {
          wbConfigDir = getValue(wbArg);
        }
        wbOptions.push_back(wbArg);
      }
    }
    fclose(handle);
  }
}

void LauncherOptions::saveJavaHome(string newHome)
{
  WritePrivateProfileString (TEXT("Workbench"), TEXT("javaHome"), newHome.c_str(), configFileName.c_str() );
}

void LauncherOptions::calculateMemory()
{
  if (memorySpecified) return;

  string sizeMb;
  if (javaFinder.is64BitJava())
  {
    sizeMb = to_string((int)round(getTotalMemoryMB() * 0.65));
  }
  else
  {
    sizeMb = "1400";
  }
  Log::logMsg("Using " + sizeMb + "MB memory");
  vmOptions.push_back("-Xmx" + sizeMb + "m");
}

string LauncherOptions::getClassPath(bool forEmbedded)
{
  string cp;
  cp = mainJarFile;

  if (!forEmbedded)
  {
    // when running javaw.exe we can specify wildcards, because javaw.exe will handle them.
    return  cp  + ";" + baseDir + "\\ext\\*";
  }

  WIN32_FIND_DATA data;
  string extDir = baseDir + "\\ext";
  HANDLE hFind = FindFirstFile((extDir + "\\*.jar").c_str(), &data);
  if (hFind != INVALID_HANDLE_VALUE)
  {
    do
    {
      string fname = data.cFileName;
      cp += ";" + extDir + "\\" + fname;
    } while (FindNextFile(hFind, &data));
    FindClose(hFind);
  }
  return cp;
}

string LauncherOptions::getJavaHome()
{
  return javaHome;
}

void LauncherOptions::convertOldINI()
{
  if (FileExists(configFileName)) return;

  string fname = baseDir + "\\sqlworkbench64.ini";
  if (FileExists(fname))
  {
    convertOldINI(fname);
  }
  else
  {
    fname = baseDir + "\\sqlworkbench.ini";
    if (FileExists(fname))
    {
      convertOldINI(fname);
    }
  }
}

void LauncherOptions::convertOldINI(string oldFilename)
{
  FILE *handle = fopen(oldFilename.c_str(), "r");

  if (handle == NULL) return;

  Log::logMsg("Converting old file " + oldFilename + " to " + configFileName);

  string oldHome;
  list<string> options;
  char lineBuffer[1024];
  while( fgets(lineBuffer, 1024, handle) != NULL)
  {
    string line = string(lineBuffer);
    if (startsWithIgnoreCase(line, "vm.location"))
    {
      oldHome = getValue(line);
      int pos = oldHome.find("\\bin");
      if (pos > 0)
      {
        oldHome = oldHome.substr(0,pos);
        pos = oldHome.find("\\jre");
        if (pos > 0)
        {
          oldHome = oldHome.substr(0,pos);
        }
      }
    }
    else if (startsWithIgnoreCase(line, "vm.heapsize.preferred"))
    {
      string mem = "-Xmx" + getValue(line) + "m";
      options.push_back(mem);
    }
    else if (startsWithIgnoreCase(line, "vmarg"))
    {
      options.push_back(getValue(line));
    }
  }
  fclose(handle);

  if (!oldHome.empty())
  {
    saveJavaHome(oldHome);
  }

  int i=1;
  for (string parm : options)
  {
    string item = "vmarg." + to_string(i);
    WritePrivateProfileString (TEXT("Java"), item.c_str(), parm.c_str(), configFileName.c_str());
    i++;
  }
  MoveFile(oldFilename.c_str(), (oldFilename + "_obsolete").c_str());
}
