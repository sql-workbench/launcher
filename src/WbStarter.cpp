/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2022, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
#include <string>
#include <list>

#include <jni.h>
#include <shlobj.h>
#include <windows.h>
#include <winuser.h>
#include <commctrl.h>

#include "WbStarter.h"
#include "LauncherOptions.h"
#include "DialogChoice.h"
#include "Log.h"

using namespace std;

typedef jint (CALLBACK *CreateJavaVM)(JavaVM **jvm, JNIEnv **env, void *args);
const char* MAIN_CLASS = "workbench/WbManager";

WbStarter::WbStarter(LauncherOptions cmdLineOptions, HANDLE mutex)
{
  is64bit = (INTPTR_MAX == INT64_MAX);
  options = cmdLineOptions;
  mutexHandle = mutex;
}

WbStarter::~WbStarter()
{
  if (jvm)
  {
    jvm->DestroyJavaVM();
  }

  if (jvmOptions)
  {
    delete[] jvmOptions;
  }

  if (hDll)
  {
    FreeLibrary(hDll);
  }

  if (mutexHandle)
  {
    ReleaseMutex(mutexHandle);
    CloseHandle(mutexHandle);
  }
}

void WbStarter::startWorkbench()
{
  startJvm();
}

string WbStarter::selectJavaHomeFolder()
{
  TCHAR path[MAX_PATH];

  const char * path_param = "";

  BROWSEINFO bi = { 0 };
  bi.lpszTitle  = "Please select the installation folder of a Java 11 (or higher) runtime";
  bi.ulFlags    = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
  bi.lpfn       = NULL;
  bi.lParam     = (LPARAM) path_param;

  LPITEMIDLIST pidl = SHBrowseForFolder ( &bi );

  if (pidl != 0)
  {
    //get the name of the folder and put it in path
    SHGetPathFromIDList(pidl, path);

    //free memory used
    IMalloc * imalloc = 0;
    if (SUCCEEDED( SHGetMalloc(&imalloc)))
    {
      imalloc->Free(pidl);
      imalloc->Release();
    }
    return path;
  }
  return "";
}

void WbStarter::startJvm()
{
  if (options.javaFinder.getJavaHome().empty())
  {
    SelectionDialog dialog;
    dialog.baseDir = options.getBaseDir();
    dialog.useWbDownload = options.useWorkbenchJreDownload();
    int choice = dialog.selectAction();

    if (choice == ACTION_SELECT_HOME)
    {
      string home = selectJavaHomeFolder();
      if (home.empty())
      {
        return;
      }

      if (!options.javaFinder.setJavaHome(home))
      {
        MessageBox(NULL, "The selected folder does not contain a Java 11 installation", "SQL Workbench/J", MB_OK | MB_ICONERROR);
        return;
      }
      options.saveJavaHome(home);
    }
    if (choice == ACTION_CANCEL)
    {
      return;
    }

    if (choice == ACTION_DOWNLOAD)
    {
      // TODO show dialog/progress
      // the dialog will have downloaded and unpacked the JRE
      string jreDir = options.getBaseDir() + "\\jre";
      options.javaFinder.setJavaHome(jreDir);
      Log::logMsg("Using downloaded local Java runtime from " + jreDir + ", with JVM: " + options.javaFinder.getJvmDLL());
    }
  }

  if (!options.javaFinder.canStartInProcJVM())
  {
    if (!is64bit && options.javaFinder.is64BitJava())
    {
      Log::logError("The 32bit launcher can't start an in-process JVM for a 64bit Java runtime");
      string msg = "The Java runtime at: \"" + options.javaFinder.getJavaHome() + "\" is a 64bit Java.\nIt is recommended to use SQLWorkbench64.exe instead";
      MessageBox(NULL, msg.c_str(), "SQL Workbench/J", MB_OK | MB_ICONWARNING);
    }
    else if (is64bit && !options.javaFinder.is64BitJava())
    {
      Log::logError("The 64bit launcher can't start an in-process JVM for a 32bit Java runtime");
      string msg = "The Java runtime at: \"" + options.javaFinder.getJavaHome() + "\" is a 32bit Java.\nIt is recommended to use SQLWorkbench.exe instead";
      MessageBox(NULL, msg.c_str(), "SQL Workbench/J", MB_OK | MB_ICONWARNING);
    }
  }
  else
  {
    bool started = initVM();
    if (started) return;
    Log::logError("Could not start in-process JVM. Starting javaw.exe");
  }

  startJavaProcess();
}

void WbStarter::prepareDllPath()
{
  string binPath = options.javaFinder.getBinPath();
  typedef BOOL (WINAPI *LPFNSetDllDirectory)(LPCTSTR lpPathname);
  HINSTANCE hKernel32 = GetModuleHandle("kernel32");
  LPFNSetDllDirectory lpfnSetDllDirectory = (LPFNSetDllDirectory)GetProcAddress(hKernel32, "SetDllDirectoryA");
  if (lpfnSetDllDirectory != NULL)
  {
    lpfnSetDllDirectory(binPath.c_str());
  }
}

bool WbStarter::initVM()
{
  string jvmDll = options.javaFinder.getJvmDLL();
  prepareDllPath();

  hDll = LoadLibrary(jvmDll.c_str());
  if (!hDll)
  {
    Log::logError("Could not load JVM dll: " + options.javaFinder.getJvmDLL());
    return false;
  }

  CreateJavaVM createJavaVM = (CreateJavaVM) GetProcAddress(hDll, "JNI_CreateJavaVM");
  if (!createJavaVM)
  {
    Log::logError("Could not get proc address of JNI_CreateJavaVM");
    return false;
  }

  list<string> vmOptions = options.getVmOptions();
  int numOptions = vmOptions.size() + 1;
  jvmOptions = new JavaVMOption[numOptions];
  int i = 0;

  string cp = "-Djava.class.path=" + options.getClassPath(true);
  Log::logMsg("Using classpath parameter: " + cp);

  // when using (char *) the option isn't  recognized by the JVM
  // so I need to use strdup()
  jvmOptions[i].optionString = strdup(cp.c_str());
  jvmOptions[i].extraInfo = 0;
  i++;

  for (string parm : options.getVmOptions())
  {
    Log::logMsg("Adding JVM option: " + parm);
    jvmOptions[i].optionString = strdup(parm.c_str());
    jvmOptions[i].extraInfo = 0;
    i++;
  }

  JavaVMInitArgs jvmArgs;

  jvmArgs.options = jvmOptions;
  jvmArgs.nOptions = numOptions;
  jvmArgs.version = JNI_VERSION_10;
  jvmArgs.ignoreUnrecognized = JNI_TRUE;

  int createResult = createJavaVM(&jvm, &env, &jvmArgs);
  if (createResult != JNI_OK)
  {
    Log::logError("JVM creation failed, error code: " + createResult);
    return false;
  }
  DWORD version = env -> GetVersion();
  Log::logMsg("JVM created from " + jvmDll + ", JNI version=" + to_string(HIWORD(version)) + "." + to_string(LOWORD(version))  );

  jclass mainClass = env->FindClass(MAIN_CLASS);
  if (!mainClass)
  {
    Log::logJavaError("did not find main class", env);
    return false;
  }

  jmethodID mainMethod = env->GetStaticMethodID(mainClass, "main", "([Ljava/lang/String;)V");
  if (!mainMethod)
  {
    Log::logJavaError("could not find main method", env);
    return false;
  }

  jclass javaStringClass = env->FindClass("java/lang/String");
  if (!javaStringClass)
  {
    Log::logJavaError("could not find String.class", env);
    return false;
  }

  jstring javaString = env->NewStringUTF("");

  list<string> wbArgs = options.getWbOptions();
  jobjectArray argsArray = env->NewObjectArray(wbArgs.size(), javaStringClass, javaString);
  if (!argsArray)
  {
    Log::logJavaError("could not create args array", env);
    return false;
  }

  i = 0;
  for (const string &parm : options.getWbOptions())
  {
    jstring argString = createJavaString(javaStringClass, (char*)parm.c_str());
    env->SetObjectArrayElement(argsArray, i, argString);
    i++;
  }

  env->CallStaticVoidMethod(mainClass, mainMethod, argsArray);
  return true;
}

jstring WbStarter::createJavaString(jclass aStringClass, const char *str)
{
  if (env->EnsureLocalCapacity(2) < 0)
  {
    Log::logError("could not create Java String because not enough memory!");
    return NULL; /* out of memory error */
  }

  jstring result;
  jbyteArray bytes = 0;

  int len = strlen(str);
  bytes = env->NewByteArray(len);
  if (bytes != NULL)
  {
    env->SetByteArrayRegion(bytes, 0, len, (jbyte *)str);
    jmethodID MID_String_init = env->GetMethodID(aStringClass, "<init>", "([B)V");
    result = (jstring)env->NewObject(aStringClass, MID_String_init, bytes);
    env->DeleteLocalRef(bytes);
    return result;
  }
  return NULL;
}

/**
  Start SQL Workbench by running javaw.exe as a separate process.
  This is done if the found JVM doesn't match the "bits" of the launcher process.
*/
void WbStarter::startJavaProcess()
{
  STARTUPINFO info={sizeof(info)};
  PROCESS_INFORMATION processInfo;

  string cmdLine = " -cp " + options.getClassPath(false) + " workbench.WbStarter ";

  for (string parm : options.getVmOptions())
  {
    cmdLine += " " + parm;
  }

  for (string parm : options.getWbOptions())
  {
    cmdLine += " " + parm;
  }

  Log::logMsg("Starting Java using: " + options.javaFinder.getJavaExe() + " " + cmdLine);

  LPSTR cmd = const_cast<char *>(cmdLine.c_str());

  int result = CreateProcess(options.javaFinder.getJavaExe().c_str(), cmd, NULL, NULL, TRUE, 0, NULL, NULL, &info, &processInfo);
  if (result)
  {
    WaitForSingleObject(processInfo.hProcess, INFINITE);
    CloseHandle(processInfo.hProcess);
    CloseHandle(processInfo.hThread);
  }
  else
  {
    DWORD errorMessageID = GetLastError();
    if(errorMessageID > 0)
    {
      LPSTR messageBuffer = nullptr;
      size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                   NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

      std::string message(messageBuffer, size);
      LocalFree(messageBuffer);
      Log::logError("could not start java process: " + message);
    }
  }
}

