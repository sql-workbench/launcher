/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2022, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
#include <windows.h>
#include <winuser.h>
#include <conio.h>
#include <tchar.h>
#include <fileapi.h>
#include <tlhelp32.h>
#include <psapi.h>

#include "LauncherOptions.h"
#include "WbStarter.h"

BOOL CALLBACK enumCallback(HWND hWnd, LPARAM lParam)
{
	DWORD procId = 0;
	GetWindowThreadProcessId(hWnd, &procId);
	if((DWORD)lParam == procId)
  {
		WINDOWINFO wi;
		wi.cbSize = sizeof(WINDOWINFO);
		GetWindowInfo(hWnd, &wi);

		if((wi.dwStyle & WS_VISIBLE) != 0)
    {
      BringWindowToTop(hWnd);
      SetForegroundWindow(hWnd);
      SetFocus(hWnd);
			return FALSE;
		}
	}
	return TRUE;
}

void activateRunningInstance()
{
	DWORD myProcId = GetCurrentProcessId();
  DWORD aProcesses[1024], cbNeeded, cProcesses;

  TCHAR exeName[MAX_PATH];
  GetModuleFileName(0, exeName, MAX_PATH);

  TCHAR runningExe[MAX_PATH];

  if ( !EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded) )
  {
    return;
  }

  HANDLE hProcess;
  cProcesses = cbNeeded / sizeof(DWORD);

  for (unsigned int i = 0; i < cProcesses; i++ )
  {
    if( aProcesses[i] != 0 )
    {
      hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,	FALSE, aProcesses[i]);
      GetModuleFileNameEx(hProcess, 0, runningExe, MAX_PATH);
      CloseHandle(hProcess);

      if (myProcId != aProcesses[i] && _tcsicmp(exeName, runningExe) == 0)
      {
        EnumWindows(enumCallback, aProcesses[i]);
        return;
      }
    }
  }
}

int main( int argc, char* argv[] )
{
  LauncherOptions options(argc, argv);

  HANDLE mutex = 0;
  if (options.useSingleInstance())
  {
    string mutexName = options.getMutexName();
    mutex = CreateMutex(NULL, TRUE, strdup(mutexName.c_str()));

    if (ERROR_ALREADY_EXISTS == GetLastError())
    {
      activateRunningInstance();
      return (1);
    }
  }

  WbStarter vm(options, mutex);
  vm.startWorkbench();
  return 0;
}

