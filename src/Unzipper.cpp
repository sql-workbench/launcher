/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2022, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
#include <string>
#include <set>
#include <zlib.h>
#include <stdio.h>
#include <fileapi.h>

#include "Unzipper.h"
#include "unzip.h"

using namespace std;

Unzipper::Unzipper(string fileName)
{
  archiveName = fileName;
}

Unzipper::~Unzipper()
{
  close();
}

void Unzipper::close()
{
  if (zipHandle != 0)
  {
    CloseZip(zipHandle);
    zipHandle = 0;
  }
}

bool Unzipper::singleSubDir()
{
  zipHandle = OpenZip(archiveName.c_str(),0);
  ZIPENTRY ze;
  GetZipItem(zipHandle, -1, &ze);

  int numitems = ze.index;
  int topLevelFiles = 0;
  std::set<std::string> topLevelDirs;

  for (int i=0; i < numitems; i++)
  {
    GetZipItem(zipHandle, i, &ze);
    if ((ze.attr & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
    {
      string dir = string(ze.name);
      size_t pos = dir.find("/");
      if (pos == string::npos)
      {
        topLevelDirs.insert(dir);
      }
      else
      {
        dir = dir.substr(0,pos);
        topLevelDirs.insert(dir);
      }
    }
    else
    {
      string fname = string(ze.name);
      // only count files without a directory
      if (fname.find("/") == string::npos)
      {
        topLevelFiles ++;
      }
    }
    if (topLevelDirs.size() > 1 || topLevelFiles > 0) break;
  }
  close();
  return topLevelDirs.size() == 1 && topLevelFiles == 0;
}

string Unzipper::unzipTo(string dir)
{
  zipHandle = OpenZip(archiveName.c_str(),0);
  SetUnzipBaseDir(zipHandle, dir.c_str());

  ZIPENTRY ze;
  GetZipItem(zipHandle, -1, &ze);
  string firstDir;

  int numitems=ze.index;
  for (int zi=0; zi<numitems; zi++)
  {
    GetZipItem(zipHandle, zi, &ze);
    UnzipItem(zipHandle, zi, ze.name);
    if (firstDir.empty() && (ze.attr & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
    {
      firstDir = string(ze.name);
    }
  }
  close();

  return firstDir;
}
