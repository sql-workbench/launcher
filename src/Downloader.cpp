/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2022, Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
#include <string>
#include <system_error>

#include <windows.h>
#include <winhttp.h>
#include "Downloader.h"
#include "utils.h"
#include "Log.h"

static const std::string DEFAULT_HOST_NAME = "api.adoptopenjdk.net";
static const std::string WB_HOST_NAME = "www.sql-workbench.eu";

Downloader::Downloader(bool useWorkbenchDist)
{
  bool is64BitExe = (INTPTR_MAX == INT64_MAX);
  if (useWorkbenchDist)
  {
    hostname = WB_HOST_NAME;
    if (is64BitExe)
    {
      url = "/jre/jre_win64.zip";
    }
    else
    {
      url = "/jre/jre_win32.zip";
    }
  }
  else
  {
    hostname = DEFAULT_HOST_NAME;
    if (is64BitExe)
    {
      url = "/v3/binary/latest/16/ga/windows/x64/jre/hotspot/normal/adoptopenjdk?project=jdk";
    }
    else
    {
      url = "/v3/binary/latest/16/ga/windows/x32/jre/hotspot/normal/adoptopenjdk?project=jdk";
    }
  }
}

Downloader::~Downloader()
{
  closeConnection();
}

std::string GetLastErrorAsString(DWORD errorCode)
{
  std::string message;

  if (errorCode == 0)
  {
    return std::string(); //No error message has been recorded
  }
  else
  {
    message = std::system_category().message(errorCode);
  }

  if (message.empty())
  {
    return to_string(errorCode);
  }
  return message + " (" + to_string(errorCode) + ")";
}

void Downloader::closeConnection()
{
  if (hRequest) WinHttpCloseHandle(hRequest);
  if (hConnection) WinHttpCloseHandle(hConnection);
  if (hSession) WinHttpCloseHandle(hSession);
  hRequest = NULL;
  hConnection = NULL;
  hSession = NULL;
}

bool Downloader::downloadJRE(string outputfile)
{
  DWORD dwSize = 0;
  DWORD dwDownloaded = 0;

  LPCWSTR w_hostname = toLPCWSTR(hostname.c_str());
  LPCWSTR w_url = toLPCWSTR(url.c_str());

  Log::logMsg("Downloading JRE using https://" + hostname + url + " to \"" + outputfile + "\"");

  hSession = WinHttpOpen(L"SQL Workbench/J Downloader", WINHTTP_ACCESS_TYPE_NO_PROXY, NULL, NULL, 0);
  hConnection = WinHttpConnect(hSession, w_hostname, INTERNET_DEFAULT_HTTPS_PORT, 0);
  hRequest = WinHttpOpenRequest( hConnection, L"GET", w_url, NULL, WINHTTP_NO_REFERER, WINHTTP_DEFAULT_ACCEPT_TYPES, WINHTTP_FLAG_SECURE);

  DWORD dwFlags = SECURITY_FLAG_IGNORE_UNKNOWN_CA |
                  SECURITY_FLAG_IGNORE_CERT_WRONG_USAGE |
                  SECURITY_FLAG_IGNORE_CERT_CN_INVALID |
                  SECURITY_FLAG_IGNORE_CERT_DATE_INVALID;

  BOOLAPI result = WinHttpSetOption(hRequest, WINHTTP_OPTION_SECURITY_FLAGS, &dwFlags, sizeof(dwFlags));
  if (!result)
  {
    Log::logError("WinHttpSetOption() returned error: " + GetLastErrorAsString(GetLastError()));
  }

  result = WinHttpSendRequest(hRequest, WINHTTP_NO_ADDITIONAL_HEADERS, 0, WINHTTP_NO_REQUEST_DATA, 0, 0, 0);
  if (!result)
  {
    Log::logError("WinHttpSendRequest() returned error: " + GetLastErrorAsString(GetLastError()));
    delete w_hostname;
    delete w_url;
    return false;
  }

  LPSTR pszOutBuffer;
  FILE * pFile = 0;

  bool success = false;
  if (result)
  {
    result = WinHttpReceiveResponse(hRequest, NULL);
    if (result)
    {
      pFile = fopen(outputfile.c_str(), "w+b");
      do
      {
        dwSize = 0;
        if (!WinHttpQueryDataAvailable( hRequest, &dwSize))
        {
          Log::logError("WinHttpQueryDataAvailable() returned error: " + GetLastErrorAsString(GetLastError()));
          break;
        }
        pszOutBuffer = new char[dwSize+1];
        ZeroMemory(pszOutBuffer, dwSize+1);
        if (!WinHttpReadData( hRequest, (LPVOID)pszOutBuffer, dwSize, &dwDownloaded))
        {
          Log::logError("WinHttpReadData() returned error: " + GetLastErrorAsString(GetLastError()));
          delete [] pszOutBuffer;
          break;
        }
        fwrite(pszOutBuffer, (size_t)dwDownloaded, (size_t)1, pFile);
        delete [] pszOutBuffer;
      }
      while (dwSize>0);
      success = true;
    }
    else
    {
      Log::logError("WinHttpReceiveResponse() returned error: " + GetLastErrorAsString(GetLastError()));
    }
  }

  if (w_hostname) delete w_hostname;
  if (w_url) delete w_url;
  if (pFile) fclose(pFile);
  closeConnection();
  return success;
}
