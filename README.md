# SQL Workbench/J Windows Launcher

This is the public Git repository for the SQL Workbench/J Windows launcher.

Homepage and downloads are here: http://www.sql-workbench.eu

License information can be found at: https://www.sql-workbench.eu/manual/license.html

