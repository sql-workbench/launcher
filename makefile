# Makefile to build the 64bit version of the launcher

WORKDIR = %cd%

# GCC_DIR = D:\etc\MinGW64\bin
# JDK_DIR = D:\etc\jdk-11

# GCC_DIR should be passed as the "bin" directory of the MinGW installation to be used
# JDK_DIR should be passed as the "Java home", if it's not defined JAVA_HOME is used

TARGET = x64
CC = $(GCC_DIR)\gcc.exe
CXX = $(GCC_DIR)\g++.exe
AR = $(GCC_DIR)\ar.exe
LD = $(GCC_DIR)\g++.exe
WINDRES = $(GCC_DIR)\windres.exe

ifdef JDK_DIR
  JVM_LIB = $(JDK_DIR)\lib\jvm.lib
else
  JVM_LIB = $(JAVA_HOME)\lib\jvm.lib
endif 
  
INC = -Iinclude
CFLAGS_ALL =  -Os -Wno-return-type -Wno-attributes -std=c++11 -Wno-unused-function -Wno-unused-value -Wno-misleading-indentation -Wno-shadow -fpermissive -pthread -mthreads
LDFLAGS_ALL = -pthread -static-libstdc++ -static-libgcc -static -lpthread -mwindows

ifeq ($(TARGET),x64)
  CFLAGS = $(CFLAGS_ALL) -m64
  LDFLAGS = $(LDFLAGS_ALL) -m64
  OUT_RELEASE = bin\SQLWorkbench64.exe
else
  CFLAGS = $(CFLAGS_ALL) -m32 -DWIN32_TARGET 
  LDFLAGS = $(LDFLAGS_ALL) -m32
  OUT_RELEASE = bin\SQLWorkbench.exe
endif

RESINC = 
LIBDIR = 
LIB =  -lgdi32 -luser32 -lkernel32 -lcomctl32 -lversion -lpthread -lpsapi -lwinhttp ${JVM_LIB}

ifeq ($(TARGET),x64)
  OBJDIR = obj\\64
else
  OBJDIR = obj\\32
endif

OBJ_RELEASE = $(OBJDIR)\\unzip.o $(OBJDIR)\\main.o $(OBJDIR)\\WbStarter.o $(OBJDIR)\\Unzipper.o $(OBJDIR)\\Log.o $(OBJDIR)\\LauncherOptions.o $(OBJDIR)\\JavaFinder.o $(OBJDIR)\\Downloader.o $(OBJDIR)\\DialogChoice.o $(OBJDIR)\\resource.o

all: release 

clean: clean_release

before_release: 
	cmd /c if not exist bin md bin
	cmd /c if not exist $(OBJDIR) md $(OBJDIR)

after_release: 

release: before_release out_release after_release

out_release: before_release $(OBJ_RELEASE)
	$(LD) $(LIBDIR) -o $(OUT_RELEASE) $(OBJ_RELEASE)  $(LDFLAGS) $(LIB)

$(OBJDIR)\\unzip.o: src\\unzip.cpp
	$(CXX) $(CFLAGS) $(INC) -c src\\unzip.cpp -o $(OBJDIR)\\unzip.o

$(OBJDIR)\\main.o: src\\main.cpp
	$(CXX) $(CFLAGS) $(INC) -c src\\main.cpp -o $(OBJDIR)\\main.o

$(OBJDIR)\\WbStarter.o: src\\WbStarter.cpp
	$(CXX) $(CFLAGS) $(INC) -c src\\WbStarter.cpp -o $(OBJDIR)\\WbStarter.o

$(OBJDIR)\\Unzipper.o: src\\Unzipper.cpp
	$(CXX) $(CFLAGS) $(INC) -c src\\Unzipper.cpp -o $(OBJDIR)\\Unzipper.o

$(OBJDIR)\\Log.o: src\\Log.cpp
	$(CXX) $(CFLAGS) $(INC) -c src\\Log.cpp -o $(OBJDIR)\\Log.o

$(OBJDIR)\\LauncherOptions.o: src\\LauncherOptions.cpp
	$(CXX) $(CFLAGS) $(INC) -c src\\LauncherOptions.cpp -o $(OBJDIR)\\LauncherOptions.o

$(OBJDIR)\\JavaFinder.o: src\\JavaFinder.cpp
	$(CXX) $(CFLAGS) $(INC) -c src\\JavaFinder.cpp -o $(OBJDIR)\\JavaFinder.o

$(OBJDIR)\\Downloader.o: src\\Downloader.cpp
	$(CXX) $(CFLAGS) $(INC) -c src\\Downloader.cpp -o $(OBJDIR)\\Downloader.o

$(OBJDIR)\\DialogChoice.o: src\\DialogChoice.cpp
	$(CXX) $(CFLAGS) $(INC) -c src\\DialogChoice.cpp -o $(OBJDIR)\\DialogChoice.o

$(OBJDIR)\\resource.o: resource.rc
	$(WINDRES) -i resource.rc -J rc -o $(OBJDIR)\\resource.o -O coff $(INC)

clean_release: 
	cmd /c del /f /q $(OUT_RELEASE)
	cmd /c del /f /q $(OBJDIR)


.PHONY: before_release after_release clean_release

