/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2025 Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
#ifndef LOG_H
#define LOG_H

#include <string>
#include <jni.h>

class Log
{
  public:
    static void setLogfileName(std::string fname);
    static void logMsg(std::string msg);
    static void logError(std::string errorMsg);
    static void logJavaError(std::string msg, JNIEnv *env);
    static void deleteLogfile();
    static void setWriteLogFile(bool flag);
    static bool canWrite(std::string fname);

  protected:

  private:
    static void writeMsg(std::string msg);
};

#endif // LOG_H
