/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2025 Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
#ifndef LAUNCHEROPTIONS_H
#define LAUNCHEROPTIONS_H

#include <string>
#include <list>
#include <jni.h>

#include "JavaFinder.h"

using namespace std;

class LauncherOptions
{
  public:
    LauncherOptions();
    LauncherOptions(int argc, char* argv[]);

    bool shouldLog();
    bool shouldStartInProcessVM();
    bool useSingleInstance();
    bool useWorkbenchJreDownload();
    void logMsg(string msg);
    void logError(string msg);
    void logJavaError(string msg, JNIEnv *env);
    string getJavaHome();
    string getBaseDir();
    string getMutexName();
    string getClassPath(bool forEmbedded);
    void saveJavaHome(string javaHome);
    list<string> getVmOptions();
    list<string> getWbOptions();
    JavaFinder javaFinder;

  protected:

  private:
    void parseCommandLine();
    list<string> wbOptions;
    list<string> vmOptions;
    list<string> launcherOptions;
    void readConfigFile();
    void calculateMemory();
    bool startsWithIgnoreCase(string input, string toCheck);
    bool startsWith(string input, string toCheck);
    string getBaseName(string exename);
    string getValue(string input);
    string expandEnv(std::string text);

    string javaHome;
    string baseDir;
    string configFileName;
    string mainJarFile;
    string baseName;
    string wbConfigDir;

    bool workbenchJreDownload = true;
    bool singleInstance = true;
    bool memorySpecified = false;
    bool startInProcessJVM = true;
    void convertOldINI();
    void convertOldINI(string oldFileName);
};

#endif
