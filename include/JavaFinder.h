/*
 * This file is part of SQL Workbench/J, https://www.sql-workbench.eu
 *
 * Copyright 2002-2025 Thomas Kellerer
 *
 * Licensed under a modified Apache License, Version 2.0
 * that restricts the use for certain governments.
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *     https://www.sql-workbench.eu/manual/license.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * To contact the author please send an email to: support@sql-workbench.eu
 *
 */
#ifndef JAVAFINDER_H
#define JAVAFINDER_H

#include <string>
#include <list>

#include <windows.h>

using namespace std;

enum Location { localJRE, wbEnv, javaHomeEnv, registry, systemPath, unknown};

class JavaFinder
{
  public:
    JavaFinder();
    JavaFinder(string basedir, string userSuppliedHome);

    bool canStartInProcJVM();
    bool is64BitJava();
    string getJavaHome();
    string getJvmDLL();
    string getBinPath();
    string getJavaExe();
    string getJavaHomeFromRegistry();
    string searchPath();
    int parseVersionString(string version);
    bool containsJava(string homeDir);
    bool setJavaHome(string home);
    int getJavaVersion(string home);
    list<string> split(string input, char delimiter);
    int getFileVersion(string fname);
    Location getFoundLocation();
  protected:

  private:
    bool is64BitExe;
    bool findJava();

    string readRegistry(HKEY key);
    bool is64BitFile(string fileName);
    void buildBinaryPath();
    bool canUseInProcJVM(string dll);
    string makeJavaBin(string home);

    string binPath;
    string jvmDLL;
    string javaExe;
    string javaHome;
    string exeDir;
    Location foundLocation = unknown;
};

#endif // JAVAFINDER_H
